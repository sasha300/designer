<html>
<head>
	<title>Дизайнер ver 1.6</title>
	
	<style>
   .btn {
		display: inline-block; /* Строчно-блочный элемент */
		background: #ecebed; /* Серый цвет фона */
		color: #000000; /* Белый цвет текста */
		padding: 5px; /* Поля вокруг текста */
		text-decoration: none; /* Убираем подчёркивание */
		border-radius: 1px; /* Скругляем уголки */
		font-family: Arial;
	}
	
	.color {
		color: #0000FF
	}
	
	body {
    background-image: url(tx_wood_v3.jpg); /* Путь к фоновому изображению */
	}
	</style>
</head>
<body>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

 
<?php
include 'link.php';

// условие, если название и сумма заполнены
if (isset($_GET['title']) && $_GET['title'] !== '' && isset($_GET['value']) && $_GET['value'] !== ''){
	switch ($_GET['action']){
		// если нажали на клавишу записать
		case 'Записать':
			$variable = $_GET['title'];
			$sql = "SELECT * FROM mytable WHERE title LIKE '$variable' OR title LIKE '% $variable' OR title LIKE '$variable %' OR title LIKE '% $variable %'" ;
			$result = mysqli_query($link, $sql) or die($link);
			echo "Повторяющиеся записи:<br>";
			if ($sql){
				foreach($result as $row)
					echo "{$row['id']}   |" . "  {$row['date']} |" . "   {$row['title']} " . "|  {$row['value']}" . "<br>";
			}
			$sql = mysqli_query($link, "INSERT INTO `mytable` (`title`, `value`) VALUES ('{$_GET['title']}', '{$_GET['value']}')");
			if ($sql)
				echo '<p align="center">Статус: Успешно!</p>';
			else
				echo '<p>Произошла ошибка: ' . mysqli_error($link) . '</p>';
			break;
		// если нажали на клавишу проверить
		case 'Проверить':
			$variable = $_GET['title'];
			$sql = "SELECT * FROM mytable WHERE title LIKE '$variable' OR title LIKE '% $variable' OR title LIKE '$variable %' OR title LIKE '% $variable %'" ;
			$result = mysqli_query($link, $sql) or die($link);
			echo "Повторяющиеся записи:<br>";
			if ($sql){
				foreach($result as $row)
					echo "{$row['id']}   |" . "  {$row['date']} |" . "   {$row['title']} " . "|  {$row['value']}" . "<br>";
			}
			break;
	}	
}
elseif (isset($_GET['title']) && $_GET['title'] !== ''){
	switch ($_GET['action']){
		case 'Записать':
			$variable = $_GET['title'];
			$sql = "SELECT * FROM mytable WHERE title LIKE '$variable' OR title LIKE '% $variable' OR title LIKE '$variable %' OR title LIKE '% $variable %'" ;
			$result = mysqli_query($link, $sql) or die($link);
			echo "Повторяющиеся записи:<br>";
			foreach($result as $row){
				echo "{$row['id']}   |" . "  {$row['date']} |" . "   {$row['title']} " . "|  {$row['value']}" . "<br>";
			}
		
			$sql = mysqli_query($link, "INSERT INTO `mytable` (`title`) VALUES ('{$_GET['title']}')");
			if ($sql)
				echo '<p align="center">Статус: Успешно!</p>';
			else
				echo '<p>Произошла ошибка: ' . mysqli_error($link) . '</p>';
			break;
		case 'Проверить':
			$variable = $_GET['title'];
			$sql = "SELECT * FROM mytable WHERE title LIKE '$variable' OR title LIKE '% $variable' OR title LIKE '$variable %' OR title LIKE '% $variable %'" ;
			$result = mysqli_query($link, $sql) or die($link);
			echo "Повторяющиеся записи:<br>";
			foreach($result as $row){
				echo "{$row['id']}   |" . "  {$row['date']} |" . "   {$row['title']} " . "|  {$row['value']}" . "<br>";
			}
			break;
		}
	}

//Удаляем, если что
if (isset($_GET['del_id']))
	$sql = mysqli_query($link, "DELETE FROM `mytable` WHERE `id` = {$_GET['del_id']}");

//Если передана переменная edit_id, то надо обновлять данные. Для начала достанем их из БД
if (isset($_GET['edit_id'])) {
  $sql = mysqli_query($link, "SELECT `title`,`value` FROM `mytable` WHERE `id`={$_GET['edit_id']}");
  $product = mysqli_fetch_array($sql);
}
?>
<!-- поле вверху страницы -->
<form action="" method="get">
<table ALIGN="center";>
	<tr>
		<td><input type="text" name="title" placeholder="Название" size="70" value="<?= isset($_GET['edit_id']) ? $product['title'] : ''; ?>"></td>
		<td><input type= "text" name="value" placeholder="Сумма" size="5" value="<?= isset($_GET['edit_id']) ? $product['value'] : ''; ?>"></td>			
		<td><p align="right"><input type="submit" name="action" value="Записать"></p></td>
		<td><p align="right"><input type="submit" name="action" value="Проверить"></p></td>
		<td><p align="left"><input type="reset" value="Очистить"></p></td>
		<td><a href="disigner.php" class="btn">В начало</a></td>
	</tr>
</table>
</form>

<table border="1" cellpadding="5" cellspacing="0" ALIGN="center" style="font-family:Arial";>
<tr>
	<th width=25px;>id</th>
	<th width=150px;>Дата и время</th>
	<th width=500px;>Название</th>
	<th width=50px;>Сумма</th>
	<th width=50px;>Баланс</th>
	<th width=50px;>Редактирование</th>
	<th width=50px;>Удаление</th>
</tr>

<?php

$sql = 'SELECT
    `mytable`.`id`,
	`mytable`.`date`,
	`mytable`.`title`,
    `mytable`.`value`,
    `mytable`.`balance`
FROM (
    SELECT 
        `mytable`.`id`,
		`mytable`.`date`,
		`mytable`.`title`,
        `mytable`.`value`,
        @Balance := @Balance + `mytable`.`value` AS `balance`
    FROM `mytable`, (SELECT @Balance := 0) AS variableInit
    ORDER BY `mytable`.`id` ASC
) AS `mytable`
ORDER BY `mytable`.`id` DESC';

$result = mysqli_query($link, $sql) or die($link);

foreach($result as $row){
	echo	'<tr>' .
				"<td align='center'>{$row['id']}</td>" .
				"<td align='center'>{$row['date']}</td>" .
				"<td>{$row['title']}</td>" .
				"<td align='center'>{$row['value']}</td>" .
				"<td align='center' class='color'><b>{$row['balance']}</b></td>" .
				"<td align='center'><a href='?edit_id={$row['id']}'>Изменить</a></td>" .
				"<td align='center'><a href='?del_id={$row['id']}'>Удалить</a></td>" .
			'</tr>';
}
?>

</table>
</body>
</html>